﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Meteorites : MonoBehaviour
{
    [Serializable]
    public class MeteoriteData
    {
        public short id;
        public bool alive;

        public float speed;
        public float size;

        public Vector3 finalDirection;
        public Vector3 initialDirection;
        public float time;
    }

    public MeteoriteData data;

    void Start()
    {
        name = "Meteorite " + data.id; 
        data.alive = true;
        data.size = Random.Range(3, 7) / 10.0f;
        data.speed = Random.Range(7, 15) + Random.Range(1, 9) / 10.0f;
        data.finalDirection = new Vector3(Random.Range(0, 100), Random.Range(0, 100), Random.Range(0, 100));
        data.initialDirection = transform.position;
        data.time = 0;

        transform.localScale = new Vector3(data.size, data.size, data.size);
    }

    void Update()
    {
        if (!data.alive) return;

        Vector3 pos = new Vector3(0, 0, 0);
        
        data.time += data.speed * 0.0001f;

        pos = Vector3.LerpUnclamped(data.initialDirection, data.finalDirection, data.time);
        transform.position = pos;
    }
}
