﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Planets : MonoBehaviour
{
    [Serializable]
    public class PlanetData
    {
        public string name;
        public short id;
        public float distanceToSun;

        public float rotationSpeed;

        public float rotationAxis;
        public float movingSpeed;

        public float size;

        public Material mat;
        public Material atMat;

        public Vector3 actualPosition;
    }

    public PlanetData data;

    public Planets.PlanetData GetPlanetData(int id)
    {
        GameObject solarSystem = GameObject.FindWithTag("SolarSystem");

        return solarSystem.GetComponent<Instancing>().planetsList[id];
    }
    void Start()
    {
        name = data.name;
        float sunSize = GameObject.FindWithTag("Sun").GetComponent<Transform>().localScale.x;
        float distanceBetweenPlanets = Random.Range(2, 6) + Random.Range(1, 9) / 10.0f;
        data.size = Random.Range(1, 3) + Random.Range(2, 8) / 10.0f;
        data.rotationSpeed = Random.Range(10, 15);
        data.movingSpeed = Random.Range(0, 2) + Random.Range(1, 9) / 10.0f;

        if (data.id == 0)
            data.distanceToSun = sunSize / 2.0f + data.size / 2.0f + distanceBetweenPlanets;
        else
            data.distanceToSun = GetPlanetData(data.id - 1).distanceToSun + data.size / 2.0f + GetPlanetData(data.id - 1).size / 2.0f + distanceBetweenPlanets;

        transform.localScale = new Vector3(data.size, data.size, data.size);
        transform.position = new Vector3(data.distanceToSun, 0, 0);
        data.actualPosition = transform.position;
        GetComponent<MeshRenderer>().material = data.mat;

        if (data.id == 2) // is earth
        {
            GameObject atmosphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            atmosphere.transform.parent = transform;
            atmosphere.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            atmosphere.transform.localPosition = new Vector3(0, 0, 0);
            atmosphere.GetComponent<MeshRenderer>().material = data.atMat;
        }
    }

    void Update()
    {
        transform.Rotate(0, data.rotationSpeed * Time.deltaTime, 0);

        data.rotationAxis += data.movingSpeed * Time.deltaTime;

        transform.position = new Vector3(data.distanceToSun * Mathf.Cos(data.rotationAxis), 0, data.distanceToSun * Mathf.Sin(data.rotationAxis));
        data.actualPosition = transform.position;
    }

    private void OnTriggerExit(Collider o)
    {
        if (o.tag == "Meteorite")
        {
            GameObject.FindWithTag("MeteoriteRain").GetComponent<MeteoriteRain>().meteoriteList.Remove(o.gameObject);
            Destroy(o.gameObject);

            data.size = data.size - 0.5f;

            if (data.size <= 0) Destroy(this);
            else transform.localScale = new Vector3(data.size, data.size, data.size);
        }
    }
}
