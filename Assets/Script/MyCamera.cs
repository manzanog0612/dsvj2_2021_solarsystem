﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour
{
    public short index = 10;
    public float speed = 1f;
    public Vector3 offset = new Vector3(0, 0, -15);
    public float time = 0.0f;
    public bool onAnimation = false;
    void Start()
    {
        transform.position = new Vector3(0,130,0);
        transform.rotation = Quaternion.Euler(90, 0, 0);
    }

    void LateUpdate()
    {
        const short shipIndex = 9;
        const short firstPosIndex = 10;
        
        short index2 = 0;
        Vector3 newPos = new Vector3(0, 0, 0);
        Vector3 oldPos = new Vector3(0, 0, 0);

        GameObject instancing = GameObject.FindWithTag("SolarSystem");

        if (Input.GetKeyDown(KeyCode.Alpha1))      { index = 0; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha2)) { index = 1; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha3)) { index = 2; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha4)) { index = 3; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha5)) { index = 4; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha6)) { index = 5; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha7)) { index = 6; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha8)) { index = 7; index2 = index; }
        else if (Input.GetKeyDown(KeyCode.Alpha9)) index = shipIndex;
        else if (Input.GetKeyDown(KeyCode.Alpha0)) index = firstPosIndex;
        else if (Input.GetKeyDown(KeyCode.Q))
        {
            if (index2 == 7) index2 = 0;
            else index2++;

            index = index2;

            if (!onAnimation)
                onAnimation = true;

            oldPos = transform.position;
            newPos = GameObject.FindWithTag("SolarSystem").GetComponent<Instancing>().planetsList[index2].actualPosition + offset;
        }

        switch (index)
        {
            case shipIndex:
                {
                    Vector3 shipPos = GameObject.FindWithTag("Ship").transform.position;
                    transform.position = new Vector3(shipPos.x, shipPos.y, shipPos.z - 15);
                    transform.LookAt(shipPos);
                }
                break;
            case firstPosIndex:
                {
                    transform.position = new Vector3(0, 130, 0);
                    transform.rotation = Quaternion.Euler(90, 0, 0);
                }
                break;
            default:
                {
                    Planets.PlanetData planetData = instancing.GetComponent<Instancing>().planetsList[index];

                    /*if (onAnimation)
                    {
                        time += Time.deltaTime;

                        transform.position = Vector3.Lerp(oldPos, newPos, time);

                        if (time> 1.0f)
                        {
                            onAnimation = false;
                            time = 0.0f;
                        }
                    }*/

                    transform.position = new Vector3((planetData.distanceToSun + 10) * Mathf.Cos(planetData.rotationAxis), 0, (planetData.distanceToSun + 10) * Mathf.Sin(planetData.rotationAxis));
                    transform.LookAt(planetData.actualPosition);
                }
                break;
        }
    }
}

/*
 case manualIndex:
                {
                    Planets.PlanetData planetData = instancing.GetComponent<Instancing>().planetsList[index2];

                    if (onAnimation)
                    {
                        if (time == 0.0f)
                        {
                            oldPos = transform.position;

                            if (index2 == 7) index2 = 0;
                            else index2++;

                            newPos = GameObject.FindWithTag("SolarSystem").GetComponent<Instancing>().planetsList[index2].actualPosition + offset;
                        }

                        if (time <= 1.0f) time += Time.deltaTime;

                        transform.position = Vector3.Lerp(oldPos, newPos, time);
                    }
                    
                    transform.LookAt(planetData.actualPosition);
                }
                break;
            default:
                { 
                    Planets.PlanetData planetData = instancing.GetComponent<Instancing>().planetsList[index];

                    transform.position = new Vector3((planetData.distanceToSun + 10) * Mathf.Cos(planetData.rotationAxis), 0, (planetData.distanceToSun + 10) * Mathf.Sin(planetData.rotationAxis));
                    transform.LookAt(planetData.actualPosition); 
                }
                    break;*/
