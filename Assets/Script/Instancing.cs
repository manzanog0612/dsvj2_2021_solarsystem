﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instancing : MonoBehaviour
{
    public List<Planets.PlanetData> planetsList;
    public Planets planetPrefab;
    public List<Material> planetsMaterial;
    public Material atmosphereMaterial;

    void Awake()
    {
        short idAux = 0;

        foreach (Planets.PlanetData p in planetsList)
        {
            p.id = idAux;
            idAux++;

            GameObject newPlanet = Instantiate(planetPrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;

            newPlanet.GetComponent<Planets>().data = p;

            newPlanet.GetComponent<Planets>().data.mat = planetsMaterial[p.id];

            newPlanet.transform.parent = GameObject.FindWithTag("SolarSystem").transform;
        }       
    }

    void Update()
    {
        
    }
}
