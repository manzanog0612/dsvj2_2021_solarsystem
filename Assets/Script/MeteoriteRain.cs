﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteoriteRain : MonoBehaviour
{
    public float timeTillNextRain;
    public float timeTillNextMeteorite;
    public float rainDurationTime;
    public bool raining;
    public short currentId = 0;

    public List<GameObject> meteoriteList;
    public Meteorites meteoritePrefab;

    void Awake()
    {
        timeTillNextRain = Random.Range(10, 20);
        timeTillNextMeteorite = 0;
        raining = false;
    }

    void Update()
    {
        Vector3 pos;

        transform.position = new Vector3(0, 0, 0);
        transform.rotation = Quaternion.Euler(0, 0, 0);

        switch (raining)
        {
            case true:
                if (rainDurationTime > 0)
                {
                    if (timeTillNextMeteorite <= 0)
                    {
                        switch (Random.Range(1, 8))
                        {
                            case 1:
                                pos = new Vector3(Random.Range(120, 170), Random.Range(120, 170), Random.Range(120, 170));
                                break;
                            case 2:
                                pos = new Vector3(Random.Range(120, 170), Random.Range(120, 170), Random.Range(-120, -170));
                                break;
                            case 3:
                                pos = new Vector3(Random.Range(120, 170), Random.Range(-120, -170), Random.Range(120, 170));
                                break;
                            case 4:
                                pos = new Vector3(Random.Range(-120, -170), Random.Range(120, 170), Random.Range(120, 170));
                                break;
                            case 5:
                                pos = new Vector3(Random.Range(-120, -170), Random.Range(-120, -170), Random.Range(-120, -170));
                                break;
                            case 6:
                                pos = new Vector3(Random.Range(-120, -170), Random.Range(-120, -170), Random.Range(120, 170));
                                break;
                            case 7:
                                pos = new Vector3(Random.Range(-120, -170), Random.Range(120, 170), Random.Range(-120, -170));
                                break;
                            case 8:
                                pos = new Vector3(Random.Range(120, 170), Random.Range(-120, -170), Random.Range(-120, -170));
                                break;
                            default:
                                pos = new Vector3(Random.Range(120, 170), Random.Range(-120, -170), Random.Range(-120, -170));
                                break;
                        }
                        

                        GameObject newMeteorite = Instantiate(meteoritePrefab, pos, Quaternion.identity).gameObject;
                        newMeteorite.transform.parent = GameObject.FindWithTag("MeteoriteRain").transform;
                        newMeteorite.GetComponent<Meteorites>().data.id = currentId;

                        currentId++;

                        meteoriteList.Add(newMeteorite);

                        timeTillNextMeteorite = Random.Range(2, 10) / 10.0f;
                    }
                    else
                    {
                        timeTillNextMeteorite -= Time.deltaTime;
                    }
                    rainDurationTime -= Time.deltaTime;
                }
                else
                {
                    if (meteoriteList.Count == 0)
                    {
                        raining = false;
                        timeTillNextRain = Random.Range(10, 15);
                        currentId = 0;
                    }
                }
                break;
            case false:
                timeTillNextRain -= Time.deltaTime;
                if (timeTillNextRain <= 0)
                { 
                    raining = true;
                    rainDurationTime = Random.Range(10, 15);
                }
                break;
        }
    }

    private void OnTriggerExit(Collider o)
    {
        if (o.tag == "Meteorite")
        {
            o.gameObject.GetComponent<Meteorites>().data.alive = false;
            meteoriteList.Remove(o.gameObject);
            Destroy(o.gameObject);
        }
    }
}
