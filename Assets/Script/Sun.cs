﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    [SerializeField] public float rotationSpeed = 15;
    [SerializeField] public float tiempo = 0;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);

        tiempo += Time.deltaTime;
        float val = Mathf.Sin(tiempo);
        GetComponentInChildren<Light>().intensity = 5 + val * 2;
    }
}
