﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    [SerializeField] public float speed = 20;
    void Start()
    {
        
    }

    void Update()
    {
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        Vector3 direccion = new Vector3(hor, 0, ver);

        transform.position += direccion * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider o)
    {
        if (o.tag == "Planet" || o.tag == "Sun")
            o.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, 0.5f);
    }

    private void OnTriggerExit(Collider o)
    {
        if (o.tag == "Planet" || o.tag == "Sun")
            o.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, 1);
    }
}
